﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

    public GameObject damageEffectPrefab;
    public int hp = 200;
    private int hpTotal;
    public float speed = 2;
    public int attackRate = 2;//攻击速率   多少秒攻击一次
    public float attackDistance = 2;
    public float damage = 20;   //攻击力
    private float distance = 0;
    private float attackTimer = 0;
    public float downSpeed = 1.0f;
    private float downDistance = 0;
    private Transform bloodPoint;
    private CharacterController cc;
    private Animation m_Animation;
    private GameObject hpBarGameObject;
    private UISlider hpBarSlider;
    private GameObject hudTextGameObject;
    private HUDText hudText;
    private bool isHpBarActive;

    void Start()
    {
        TransciptManager._instance.enemyList.Add(this.gameObject);
        hpTotal = hp;
        bloodPoint = transform.Find("BloodPoint");
        cc = this.GetComponent<CharacterController>();
        InvokeRepeating("CalcDistance",0 , 0.1f);
        m_Animation = this.GetComponent<Animation>();
        Transform hpBarPoint = transform.Find("HpBarPoint");
        hpBarGameObject = HpBarManager._instance.GetHpBar(hpBarPoint.gameObject);
        hpBarSlider = hpBarGameObject.transform.Find("Bg").GetComponent<UISlider>();

        hudTextGameObject = HpBarManager._instance.GetHudText(hpBarPoint.gameObject);
        hudText = hudTextGameObject.GetComponent<HUDText>();
    }

    void Update()
    {
        if (hp <= 0)
        {
            //移到地下
            downDistance +=  downSpeed * Time.deltaTime;
            transform.Translate(-transform.up * downSpeed*Time.deltaTime);
            if (downDistance > 4)
            {
                Destroy(this.gameObject);
            }
            return;
        }
        if (hp <= 0) return;
        if (distance < attackDistance)
        {
            attackTimer += Time.deltaTime;
            if (attackTimer > attackRate)
            {
                Transform player = TransciptManager._instance.player.transform;
                Vector3 targetPos = player.position;
                targetPos.y = transform.position.y;
                transform.LookAt(targetPos);
                //进行攻击
                m_Animation.Play("attack01");
                attackTimer = 0;
            }
            if (!m_Animation.Play("attack01"))
            {
                m_Animation.CrossFade("idle");
            }
        }else
        {
            m_Animation.Play("walk");
            Move();
        }
        
    }

    void Attack()
    {
        Transform player = TransciptManager._instance.player.transform;
        distance = Vector3.Distance(player.position, transform.position);
        if (distance < attackDistance)
        {
            player.SendMessage("TakeDamage", damage,SendMessageOptions.DontRequireReceiver);
        }
    }

    void Move()
    {
        Transform player = TransciptManager._instance.player.transform;
        Vector3 targetPos = player.position;
        targetPos.y = transform.position.y;
        transform.LookAt(targetPos);
        cc.SimpleMove(transform.forward * speed);
    }

    void CalcDistance()
    {
        Transform player = TransciptManager._instance.player.transform;
        distance = Vector3.Distance(player.position, transform.position);
    }

    //受到攻击调用这个方法
    //0.受到多少伤害
    //1.后退的距离
    //2.浮空的高度
    void TakeDamage(string args)
    {
        //print("args in Enemy:"+args);
        if (hp <= 0) return;
        Combo._instance.ComboPlus();
        //print("hp:" + hp);
        string[] proArray = args.Split(',');
        //for (int i = 0; i < 9; i++)
        //{
        //    print("ProArray[" + i + "]:" + proArray[i]);
        //}
        //减去伤害值
        int damage = int.Parse(proArray[0]);
        hp -= damage;
        hpBarSlider.value = (float)hp / hpTotal;
        hudText.Add("-" + damage, Color.red, 0.3f);
        //浮空和后退
        float backDistance = float.Parse(proArray[1]);
        float jumpHeight = float.Parse(proArray[2]);
        //受到攻击
        this.GetComponent<Animation>().Play("takedamage");
        
        iTween.MoveBy(this.gameObject,
            transform.InverseTransformDirection(TransciptManager._instance.player.transform.forward) * backDistance+Vector3.up*jumpHeight*2,
            0.3f);
        //出血特效实例化
            GameObject.Instantiate(damageEffectPrefab, bloodPoint.transform.position, Quaternion.identity);
        if (hp <= 0)
        {
            Dead();
            
        }
    }

    //当死亡的时候调用这个方法
    void Dead()
    {
        
        TransciptManager._instance.enemyList.Remove(this.gameObject);
        this.GetComponent<CharacterController>().enabled = false;
        GameObject.Destroy(hpBarGameObject);
        GameObject.Destroy(hudTextGameObject);
        isHpBarActive = false;
        //第一种死亡方式是播放死亡动画
        //第二种死亡方式是使用破碎效果
        //m_Animation.Play("die");
        int random = Random.Range(0, 10);
        if (random <= 7)
        {
            m_Animation.Play("die");
        }else
        {
            this.GetComponentInChildren<MeshExploder>().Explode();
            this.GetComponentInChildren<SkinnedMeshRenderer>().enabled = false;
        }
    }
}
