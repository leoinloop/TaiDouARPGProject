﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class BossBullet : MonoBehaviour {

    private float moveSpeed = 3;
    private float force = 1000;
    public float Damage
    {
        set;
        private get;
    }
    public  List<GameObject> playerList = new List<GameObject>();
    private float repeatRate = 0.5f;

	void Start () {
        InvokeRepeating("Attack", 0, 0.25f);
	}
	
	void Update () {
        transform.position += transform.forward * moveSpeed * Time.deltaTime;
	}

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            if (playerList.IndexOf(col.gameObject)<0)
            {
                playerList.Add(col.gameObject);
            }
        }
    }
    void OnTriggerExit(Collider col)
    {
        if (col.tag == "Player")
        {
            if (playerList.IndexOf(col.gameObject) >= 0)
            {
                playerList.Remove(col.gameObject);
            }
        }
    }

    void Attack()
    {
        foreach (GameObject player in playerList)
        {
            player.SendMessage("TakeDamage", Damage*repeatRate, SendMessageOptions.DontRequireReceiver);
            player.GetComponent<Rigidbody>().AddForce(transform.forward * force);
        }
    }
}
