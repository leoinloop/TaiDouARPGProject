﻿using UnityEngine;
using System.Collections;

public enum TaskType
{
    Main,   //主线任务
    Reward,//赏金任务
    Daily//日常任务
}

public enum TaskProgress
{
    NoStart,
    Accept,
    Complete,
    Reward
}

public class Task  {

    private int id;
    private TaskType taskType;
    private string name;
    private string des;
    private string icon;
    private int coin;
    private int diamond;
    private string talkNpc;
    private int idNpc;
    private int idTranscript;
    private TaskProgress taskProgress = TaskProgress.NoStart;

    public delegate void OnTaskChangeEvent();
    public event OnTaskChangeEvent OnTaskChange;

    #region getAndset
    public int Id
    {
        get
        {
            return id;
        }

        set
        {
            id = value;
        }
    }

    public string Name
    {
        get
        {
            return name;
        }

        set
        {
            name = value;
        }
    }

    public string Des
    {
        get
        {
            return des;
        }

        set
        {
            des = value;
        }
    }

    public int Coin
    {
        get
        {
            return coin;
        }

        set
        {
            coin = value;
        }
    }

    public TaskType TaskType
    {
        get
        {
            return taskType;
        }

        set
        {
            taskType = value;
        }
    }

    public int Diamond
    {
        get
        {
            return diamond;
        }

        set
        {
            diamond = value;
        }
    }

    public string TalkNpc
    {
        get
        {
            return talkNpc;
        }

        set
        {
            talkNpc = value;
        }
    }

    public int IdNpc
    {
        get
        {
            return idNpc;
        }

        set
        {
            idNpc = value;
        }
    }

    public int IdTranscript
    {
        get
        {
            return idTranscript;
        }

        set
        {
            idTranscript = value;
        }
    }

    public TaskProgress TaskProgress
    {
        get
        {
            return taskProgress;
        }

        set
        {
            if (taskProgress != value)
            {
                taskProgress = value;
                OnTaskChange();
            }
        }
    }

    public string Icon
    {
        get
        {
            return icon;
        }

        set
        {
            icon = value;
        }
    }
}
#endregion