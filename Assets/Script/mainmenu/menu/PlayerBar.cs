﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// 角色信息栏
/// </summary>
public class PlayerBar : MonoBehaviour {

    //PlayerBar 成员

    private UISprite headSprite;      //角色头像
    private UILabel nameLabel;        //角色名
    private UILabel levelLabel;       //等级
    private UILabel energyLabel;      //体力值
    private UISlider energySlider;    //体力条
    private UILabel toughenLabel;     //阅历值
    private UISlider toughenSlider;   //历练条
    private UIButton energyPlusButton;  //+体力按钮
    private UIButton toughenPlusButton; //+能力按钮

    private UIButton headButton;        //角色头像

    void Awake()
    {
        //获取
        headSprite = transform.Find("HeadSprite").GetComponent<UISprite>();
        nameLabel = transform.Find("NameLabel").GetComponent<UILabel>();
        levelLabel = transform.Find("LevelLabel").GetComponent<UILabel>();
        energyLabel = transform.Find("EnergyrProgressBar/Label").GetComponent<UILabel>();
        energySlider = transform.Find("EnergyrProgressBar").GetComponent<UISlider>();
        toughenLabel = transform.Find("ToughenProgressBar/Label").GetComponent<UILabel>();
        toughenSlider = transform.Find("ToughenProgressBar").GetComponent<UISlider>();
        energyPlusButton = transform.Find("EnergyPlusButton").GetComponent<UIButton>();
        toughenPlusButton = transform.Find("ToughenPlusButton").GetComponent<UIButton>();
        headButton = transform.Find("HeadButton").GetComponent<UIButton>();

        //通过PlayerInfo脚本的单例获取OnPlayerInfoChanged事件，添加PlayerBar的OnPlayerInfoChanged事件
        PlayerInfo._instance.OnPlayerInfoChanged += this.OnPlayerInfoChanged;

        //委托事件-headButton按钮点击触发
        EventDelegate ed = new EventDelegate(this,"OnHeadButtonClick");
        headButton.onClick.Add(ed);

    }

    void OnDestory()
    {
        //PlayerBar脚本结束
        PlayerInfo._instance.OnPlayerInfoChanged -= this.OnPlayerInfoChanged;
    }
    //当我们主角信息发生改变会触发这个方法
    void OnPlayerInfoChanged(InfoType type)
    {
        if(type==InfoType.All || type == InfoType.Name || type == InfoType.HeadPortrait || type == InfoType.Level || type == InfoType.Energy || type == InfoType.Toughen)
        {
            //PlayerBar信息更新
            UpdateShow();
        }
    }

     void UpdateShow()
    {
        PlayerInfo info = PlayerInfo._instance;
        headSprite.spriteName = info.HeadPortrait;
        levelLabel.text = info.Level.ToString();
        nameLabel.text = info.Name.ToString();
        energySlider.value = info.Energy / 100f;
        energyLabel.text = info.Energy + "/100";
        toughenSlider.value = info.Toughen / 50f;
        toughenLabel.text = info.Toughen + "/50";
    }

    //headButton按钮点击触发时间
    public void OnHeadButtonClick()
    {
        //弹出角色状态栏
        PlayerStatus._instance.Show();
    }
}
