﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class InventoryUI : MonoBehaviour {

    public static InventoryUI _instance;

    //32个Inventory-Item拖拽赋值
    public List<InventoryItemUI> itemUIList = new List<InventoryItemUI>();  //所以的物品格子
    private UIButton clearupButton;
    private UILabel inventoryLabel;

    private int count = 0;      //目前有多少个格子是有物品的

    void Awake()
    {
        _instance = this;
        InventoryManager._instance.OnInventoryChange += this.OnInventoryChange;
        clearupButton = transform.Find("ButtonClearup").GetComponent<UIButton>();
        inventoryLabel = transform.Find("InventoryLabel").GetComponent<UILabel>();

        EventDelegate ed = new EventDelegate(this, "OnClearup");
        clearupButton.onClick.Add(ed);
    }

    void OnDestory()
    {
        InventoryManager._instance.OnInventoryChange -= this.OnInventoryChange;
    }

    void OnInventoryChange()
    {
        UpdateShow();
    }

    private void UpdateShow()
    {
        int temp = 0;
        for(int i = 0; i < InventoryManager._instance.inventoryItemList.Count; i++)
        {
            
            InventoryItem it = InventoryManager._instance.inventoryItemList[i];
            if (it.IsDressed == false)   //装备标志位--未穿戴在身上
            {
                itemUIList[temp].SetInventoryItem(it);
                temp++;
            }
            else
            {
                print(it);
            }
        }
        count = temp;
        //未生成的装备的物品格子，调用Clear方法让label和sprite为空
        for (int i = temp; i < itemUIList.Count; i++)
        {
            itemUIList[i].Clear();
        }
        inventoryLabel.text = count + "/32";
    }

    public void UpdateCount()
    {
        int count = 0;
        foreach (InventoryItemUI itUI in itemUIList)
        {
            if (itUI.it != null)
            {
                count++;
            }
        }
        inventoryLabel.text = count + "/32";
    }

    //放回添加一个物品
    public void AddInventoryItem(InventoryItem it)
    {
        foreach (InventoryItemUI itUI in itemUIList)
        {
            if (itUI.it == null)
            {
                itUI.SetInventoryItem(it);
                count++;
                break;
            }
        }
        inventoryLabel.text = count + "/32";
    }

    /// <summary>
    /// 整理背包
    /// </summary>
    void OnClearup()
    {
        UpdateShow();
    }
}
