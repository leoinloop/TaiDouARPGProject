﻿using UnityEngine;
using System.Collections;

public class EquipPopup : MonoBehaviour {

    public PowerShow powerShow;

    public UIAtlas m_AntiEquipAtlas;
    public UIAtlas m_EquipAtlas;

    private InventoryItem it;
    private InventoryItemUI itUI;
    private KnapsackRoleEquip roleEquip;

    private UISprite equipSprite;
    private UILabel nameLabel;
    private UILabel qualityLabel;
    private UILabel damageLabel;
    private UILabel hpLabel;
    private UILabel powerLabel;
    private UILabel desLabel;
    private UILabel levelLabel;
    private UILabel btnLabel;

    private UIButton closeButton;
    private UIButton equipButton;
    private UIButton upgradeButton;

    private bool isLeft = true;     //装备弹出面板是显示在左边还是右边->判断装备按钮文字显示是否为卸载

    void Awake()
    {
        
        equipSprite = transform.Find("EquipBg/Sprite").GetComponent<UISprite>();
        nameLabel = transform.Find("NameLabel").GetComponent<UILabel>();
        qualityLabel = transform.Find("QualityLabel/Label").GetComponent<UILabel>();
        damageLabel = transform.Find("DamageLabel/Label").GetComponent<UILabel>();
        hpLabel = transform.Find("HpLabel/Label").GetComponent<UILabel>();
        powerLabel = transform.Find("PowerLabel/Label").GetComponent<UILabel>();
        desLabel = transform.Find("DesLabel").GetComponent<UILabel>();
        levelLabel = transform.Find("LevelLabel/Label").GetComponent<UILabel>();
        btnLabel = transform.Find("EquipButton/Label").GetComponent<UILabel>();


        closeButton = transform.Find("CloseButton").GetComponent<UIButton>();
        equipButton = transform.Find("EquipButton").GetComponent<UIButton>();
        upgradeButton = transform.Find("UpgradeButton").GetComponent<UIButton>();

        EventDelegate ed1 = new EventDelegate(this, "OnClose");
        closeButton.onClick.Add(ed1);

        EventDelegate ed2 = new EventDelegate(this, "OnEquip");
        equipButton.onClick.Add(ed2);

        EventDelegate ed3 = new EventDelegate(this, "OnUpgrade");
        upgradeButton.onClick.Add(ed3);
    }
    public void Show(InventoryItem it,InventoryItemUI itUI,KnapsackRoleEquip roleEquip, bool isLeft=true)
    {
        gameObject.SetActive(true);
        this.it = it;
        this.itUI = itUI;
        this.roleEquip = roleEquip;
        Vector3 pos = transform.localPosition;
        this.isLeft = isLeft;
        if (isLeft)
        {
            transform.localPosition = new Vector3(-Mathf.Abs(pos.x), pos.y, pos.z);
            btnLabel.text = "装备";
        }
        else
        {
            transform.localPosition = new Vector3(Mathf.Abs(pos.x), pos.y, pos.z);
            btnLabel.text = "卸下";
        }
        //点击的不是装备物品而是消耗品
        if (it.Inventroy.InventoryTYPE != InventoryType.Equip)
        {
            equipSprite.atlas = m_AntiEquipAtlas;
            equipSprite.spriteName = it.Inventroy.ICON;
        }
        else
        {
            equipSprite.atlas = m_EquipAtlas;
            equipSprite.spriteName = it.Inventroy.ICON;
        }
        //equipSprite.spriteName = it.Inventroy.ICON;
        nameLabel.text = it.Inventroy.Name;
        qualityLabel.text = it.Inventroy.Quality.ToString(); ;
        damageLabel.text = it.Inventroy.Damage.ToString();
        hpLabel.text = it.Inventroy.HP.ToString();
        powerLabel.text = it.Inventroy.Power.ToString();
        desLabel.text = it.Inventroy.Des;
        levelLabel.text = it.Level.ToString();
    }

    public void OnClose()
    {
        Close();
        gameObject.SetActive(false);

        transform.parent.SendMessage("DisableButton");
    }

    public void Close()
    {
        ClearObject();
        gameObject.SetActive(false);
    }

    //点击卸下或装备按钮的时候触发
    public void OnEquip()
    {
        int startValue = PlayerInfo._instance.GetOverPower();
        if (isLeft)     //从背包装备到身上
        {
            itUI.Clear();   //  清空该装备所在的格子
            PlayerInfo._instance.DressOn(it);
            ClearObject();
            gameObject.SetActive(false);
        }else               // 从身上脱下
        {
            roleEquip.Clear();   //把身上的装备清空
            PlayerInfo._instance.DressOff(it);
        }
        

        //物品格子不为空的计数更新
        InventoryUI._instance.SendMessage("UpdateCount");
        OnClose();
        int endValue = PlayerInfo._instance.GetOverPower();
        //战斗力改变
        powerShow.ShowPowerChange(startValue, endValue);
    }

    /// <summary>
    /// 升级按钮点击功能
    /// </summary>
    public void OnUpgrade()
    {
        int coinNeed = (it.Level + 1) * it.Inventroy.Price;
        bool isSuccess = PlayerInfo._instance.GetCoin(coinNeed);
        if (isSuccess)
        {
            it.Level += 1;
            levelLabel.text = it.Level.ToString();
        }else
        {
            //给出金币不足提示信息
            MessageManager._instance.ShowMessage("金币不足，无法显示");
        }
    }

    void ClearObject()
    {
        it = null;
        itUI = null;
    }
}
