﻿using UnityEngine;
using System.Collections;

public class Knapsack : MonoBehaviour {

    public static Knapsack _instance;

    private EquipPopup equipPopup;
    private InventoryPopup inventoryPopup;

    private UIButton saleButton;
    private UILabel priceLabel;
    private InventoryItemUI itUI;

    private TweenPosition tween;
    private UIButton closeKnapsackButton;

    void Awake()
    {
        _instance = this;
        equipPopup = transform.Find("EquipPopup").GetComponent<EquipPopup>();
        inventoryPopup = transform.Find("InventoryPopup").GetComponent<InventoryPopup>();

        saleButton = transform.Find("Inventory/ButtonSale").GetComponent<UIButton>();
        priceLabel = transform.Find("Inventory/PriceBg/Label").GetComponent<UILabel>();
        tween = transform.GetComponent<TweenPosition>();
        closeKnapsackButton = transform.Find("ButtonClose").GetComponent<UIButton>();
        DisableButton();
        

        EventDelegate ed = new EventDelegate(this,"Onsale");
        saleButton.onClick.Add(ed);

        EventDelegate ed2 = new EventDelegate(this, "OnKnapsackClose");
        closeKnapsackButton.onClick.Add(ed2);
    }


    public void Show()
    {
        tween.PlayForward();
    }

    public void Hide()
    {
        tween.PlayReverse();
    }

    public void OnInventoryClick(object[] objectArray)
    {
        InventoryItem it = objectArray[0] as InventoryItem;
        bool isLeft = (bool) objectArray[1];
        
        if (it.Inventroy.InventoryTYPE == InventoryType.Equip  )
        {
            InventoryItemUI itUI = null;
            KnapsackRoleEquip roleEquip=null;
            if (isLeft == true)
            {
                 itUI = objectArray[2] as InventoryItemUI;
            }
            else
            {
                roleEquip = objectArray[2] as KnapsackRoleEquip;
            }
            inventoryPopup.Close();
            equipPopup.Show(it, itUI, roleEquip,isLeft);
        }else {
            InventoryItemUI itUI = objectArray[2] as InventoryItemUI;
            equipPopup.Close();
            inventoryPopup.Show(it,itUI);
        }
        
        if((it.Inventroy.InventoryTYPE == InventoryType.Equip && isLeft == true)||it.Inventroy.InventoryTYPE!=InventoryType.Equip)
        {
            this.itUI = objectArray[2] as InventoryItemUI;
            EnableButton();
            priceLabel.text = (this.itUI.it.Inventroy.Price * this.itUI.it.Count).ToString();
        }

    }

    void DisableButton()
    {
        saleButton.SetState(UIButtonColor.State.Disabled, true);
        saleButton.gameObject.GetComponent<Collider>().enabled = false;
        priceLabel.text = "";
    }

    void EnableButton()
    {
        saleButton.SetState(UIButtonColor.State.Normal, true);
        saleButton.gameObject.GetComponent<Collider>().enabled = true;
    }
    
    void Onsale()
    {
        int price = int.Parse(priceLabel.text);
        PlayerInfo._instance.AddCoin(price);

        InventoryManager._instance.RemoveInventoryItem(itUI.it);
        itUI.Clear();

        equipPopup.Close();
        inventoryPopup.Close();
        DisableButton();
    }

    void OnKnapsackClose()
    {
        Hide();
    }
}
