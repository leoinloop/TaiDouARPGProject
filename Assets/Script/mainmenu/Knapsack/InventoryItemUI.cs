﻿using UnityEngine;
using System.Collections;

/// <summary>
/// 32个Inventory-item物体挂载的脚本--显示装备物品
/// </summary>
public class InventoryItemUI : MonoBehaviour {

    private UISprite sprite;
    private UILabel label;
    public InventoryItem it;

    //设置物品装备精良
    private UISprite Sprite
    {
        get
        {
            if (sprite == null)     //使用时获取
            {
                sprite = transform.Find("Sprite").GetComponent<UISprite>();
            }
            return sprite;
        }
    }
    private UILabel Label
    {
        get
        {
            if (label == null)      //获取物品个数  使用时获取
            {
                label = transform.Find("Label").GetComponent<UILabel>();
            }
            return label;
        }
    }

    /// <summary>
    /// 设置背包物品显示
    /// </summary>
    /// <param name="it"></param>
	public void SetInventoryItem(InventoryItem it)
    {
        this.it=it;
        //判断物品类型更换相应图集，NGUI图集打包最多63个精灵
        if (this.it.Inventroy.InventoryTYPE != InventoryType.Equip)
        {
            Sprite.atlas = KnapsackRole._instance.m_AntiEquipAtlas;
            Sprite.spriteName = it.Inventroy.ICON;
        }
        else
        {
            Sprite.atlas = KnapsackRole._instance.m_EquipmentAtlas;
            sprite.spriteName = it.Inventroy.ICON;
        }
        //Sprite.spriteName = it.Inventroy.ICON;

        //如果物品个数小于1则不显示，大于则显示Count属性
        if (it.Count <= 1)
        {
            Label.text = "";
        }
        else
        {
            Label.text = it.Count.ToString();
        }
        
    }

    //未获得物品的格子使用此方法保持空白
    public void Clear()
    {        
            it = null;
            Label.text = "";
            Sprite.spriteName = "bg_道具";        
    }

    public void OnClick()
    {
        if (it!=null&&it.Inventroy.InventoryTYPE==InventoryType.Equip && it!=null)
        {
            object[] objectArray = new object[3];
            objectArray[0] = it;
            objectArray[1] = true;
            objectArray[2] = this;
            transform.parent.parent.parent.SendMessage("OnInventoryClick", objectArray);
        }
        else 
        {
            object[] objectArray = new object[3];
            objectArray[0] = it;
            objectArray[1] = true;
            objectArray[2] = this;
            transform.parent.parent.parent.SendMessage("OnInventoryClick", objectArray);
        }
    }

    public void ChangeCount(int count)
    {
        if(it.Count-count<=0)
        {
            Clear();
        }
        else if (it.Count - count == 1)
        {
            label.text = "";
        }
        else
        {
            label.text = (it.Count - count).ToString();
        }
        
    }
}
