﻿using UnityEngine;
using System.Collections;

public class InventoryItem  {

    private Inventory inventroy;                //物品
    private int level;                          //物品等级
    private int count;                          //物品数量
    private bool isDressed = false;               //物品默认未穿戴

    public Inventory Inventroy
    {
        get
        {
            return inventroy;
        }
        set
        {
            inventroy = value;
        }
    }
    public int Level
    {
        get { return level; }
        set { level = value; }
    }
    public int Count
    {
        set { count = value; }
        get { return count; }
    }
    public bool IsDressed
    {
        get { return isDressed; }
        set { isDressed = value; }
    }
}
