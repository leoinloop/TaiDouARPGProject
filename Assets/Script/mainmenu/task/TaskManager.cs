﻿using UnityEngine;
using System.Collections;

public class TaskManager : MonoBehaviour {

    public static TaskManager _instance;
    public TextAsset taskinfoText;
    private ArrayList taskList = new ArrayList();
    private Task currentTask;
    private PlayerAutoMove playerAutoMove;
    public  PlayerAutoMove PlayerAutoMove
    {
        get
        {
            if(playerAutoMove==null)
            {
                playerAutoMove = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerAutoMove>();
            }
            return playerAutoMove;
        }
    }


    void Awake()
    {
        _instance = this;
        InitTask();
    }

    

    /// <summary>
    /// 初始化任务信息
    /// </summary>
    public void InitTask()
    {
        string[] tastinfoArray = taskinfoText.ToString().Split('\n');
        foreach (string str in tastinfoArray)
        {
            string[] proArray = str.Split('|');
            Task task = new Task();
            task.Id = int.Parse(proArray[0]);
            switch (proArray[1])
            {
                case "Main":
                    task.TaskType = TaskType.Main;
                    break;
                case "Reward":
                    task.TaskType = TaskType.Reward;
                    break;
                case "Daily":
                    task.TaskType = TaskType.Daily;
                    break;
            }
            task.Name = proArray[2];
            task.Icon = proArray[3];
            task.Des = proArray[4];
            task.Coin = int.Parse(proArray[5]);
            task.Diamond = int.Parse(proArray[6]);
            task.TalkNpc = proArray[7];
            task.IdNpc = int.Parse(proArray[8]);
            task.IdTranscript = int.Parse(proArray[9]);
            taskList.Add(task);
        }
    }

    public ArrayList GetTaskList()
    {
        return taskList;
    }

    /// <summary>
    /// 执行某个任务
    /// </summary>
    /// <param name="task"></param>
    public void OnExcuteTask(Task task)
    {
        currentTask = task;
        if (task.TaskProgress == TaskProgress.NoStart)      //任务未开始，导航到NPC
        {
            PlayerAutoMove.SetDestination(NPCManager._instance.GetNpcById(task.IdNpc).transform.position);
        }else if (task.TaskProgress == TaskProgress.Accept)
        {
            PlayerAutoMove.SetDestination(NPCManager._instance.transciptGo.transform.position);
        }
    }

    public void OnAcceptTask()
    {
        currentTask.TaskProgress = TaskProgress.Accept;
        //TODO 寻路到副本入口
        PlayerAutoMove.SetDestination(NPCManager._instance.transciptGo.transform.position);
        
    }

    public void OnArrriveDestination()
    {
        if (currentTask.TaskProgress == TaskProgress.NoStart)
        {
            currentTask.TaskProgress = TaskProgress.Accept;
            NPCDialogUI._instance.Show(currentTask.TalkNpc);
            
        }
        //到达副本入口TODO
    }
}
