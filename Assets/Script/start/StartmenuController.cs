﻿using UnityEngine;
using System.Collections;

public class StartmenuController : MonoBehaviour {

    public static StartmenuController _instance;   //脚本单例

    public TweenScale startpanelTween;           //开始界面缩放动画
    public TweenScale loginpanelTween;           //登录页面缩放动画
    public TweenScale registerpanelTween;        //注册面板缩放动画
    public TweenScale serverpanelTween;          //服务器面板缩放动画
    
    public TweenPosition startpanelTweenPos;      //开始界面位移动画
    public TweenPosition characterselectTweenPos;  //角色选择界面位移动画
    public TweenPosition charactershowTweenPos;    //角色展示页面位移动画
        
    public UIInput usernameInputLogin;          //login-输入用户名
    public UIInput passwordInputLogin;          //login-输入密码
    public UIInput characternameInput;          //角色展示面板-角色名输入

    public UILabel usernameLabelStart;          //开始界面-用户名
    public UILabel servernameLabelStart;        //开始界面-服务器名

    public static string username;              //用户名
    public static string password;              //用户密码
    public static ServerProperty sp;            //服务器属性脚本引用

    public UIInput usernameInputRegister;       //注册面板-用户名
    public UIInput passwordInputRegister;       //注册面板-密码
    public UIInput repasswordInputRegister;     //注册面板-确认密码

    public UIGrid serverlistGrid;               //服务列表Grid

    public GameObject serveritemRed;            //火爆服务器Item
    public GameObject serveritemGreen;          //流畅服务器Item
    

    private bool haveInitServerlist = false;        //是否已经初始化服务器列表

    public GameObject serverSelectedGo;             //用户选择的服务器

    public GameObject[] characterArry;              //角色选择数组

    public GameObject[] characterSelectedArray;     //已选择角色数组

    private GameObject characterselected;           //已选择的角色

    public Transform characterSelectedParent;

    public UILabel nameLabelCharacterSelected;
    public UILabel levelLabelCharacterSelected;

    void Awake()
    {
        _instance = this;
    }

    void Start() {
        InitServerlist();
    }
    //开始界面-游戏角色名单机事件
    public void OnUsernameClick() {
        //输入帐号进行登录 
        startpanelTween.PlayForward();
        StartCoroutine(HidePanel(startpanelTween.gameObject));
        loginpanelTween.gameObject.SetActive(true);
        loginpanelTween.PlayForward();
    }
    //服务器选择按钮事件
    public void OnServerClick() {
        //选择服务器
        startpanelTween.PlayForward();
        StartCoroutine(HidePanel(startpanelTween.gameObject));

        serverpanelTween.gameObject.SetActive(true);
        serverpanelTween.PlayForward();

        //InitServerlist();//初始化服务器列表
    }
    //开始界面-进入游戏按钮事件
    public void OnEnterGameClick() {
        //1，连接服务器，验证用户名和服务器
        //TODO

        //2,进入角色选择界面
        //TODO
        startpanelTweenPos.PlayForward();
        startpanelTweenPos.gameObject.SetActive(false);
        characterselectTweenPos.gameObject.SetActive(true);
        characterselectTweenPos.PlayForward();
    }
    //隐藏面板
    IEnumerator HidePanel(GameObject go) {
        yield return new WaitForSeconds(0.4f);
        go.SetActive(false);
    }
    //登录按钮点击事件
    public void OnLoginClick() {
        //得到用户名和密码 存储起来
        username = usernameInputLogin.value;
        password = passwordInputLogin.value;
        //返回开始界面
        loginpanelTween.PlayReverse();
        StartCoroutine(HidePanel( loginpanelTween.gameObject ));
        startpanelTween.gameObject.SetActive(true);
        startpanelTween.PlayReverse();

        usernameLabelStart.text = username;
    }
    //登录面板隐藏，显示注册面板
    public void OnRegisterShowClick() {
        //隐藏当前面板，显示注册面板
        loginpanelTween.PlayReverse();
        StartCoroutine(HidePanel(loginpanelTween.gameObject));
        registerpanelTween.gameObject.SetActive(true);
        registerpanelTween.PlayForward();
    }
    //登录界面关闭 返回开始界面
    public void OnLoginCloseClick() {
        //返回开始界面
        loginpanelTween.PlayReverse();
        StartCoroutine(HidePanel(loginpanelTween.gameObject));
        startpanelTween.gameObject.SetActive(true);
        startpanelTween.PlayReverse();
    }
    //注册面板取消按钮
    public void OnCancelClick() {
        //隐藏注册面板
        registerpanelTween.PlayReverse();
        StartCoroutine(HidePanel(registerpanelTween.gameObject));
        //显示登录面板
        loginpanelTween.gameObject.SetActive(true);
        loginpanelTween.PlayForward();
    }
    public void OnRegisterCloseClick() {
        OnCancelClick();
    }
    //注册页面登录
    public void OnRegisterAndLoginClick() {
        //1，本地校验，连接服务器进行验证
        //TODO
        //2，连接失败
        //TODO
        //3，连接成功
        //保存用户名和密码
        username = usernameInputRegister.value;
        password = passwordInputRegister.value;
        //返回到开始界面
        //隐藏注册面板
        registerpanelTween.PlayReverse();
        StartCoroutine(HidePanel(registerpanelTween.gameObject));
        startpanelTween.gameObject.SetActive(true);
        startpanelTween.PlayReverse();

        usernameLabelStart.text = username;
    }
    //初始化服务器列表
    public void InitServerlist() {
        if (haveInitServerlist) return;

        //1，连接服务器 取得游戏服务器列表信息
        //TODO
        //2，根据上面的信息 添加服务器列表

        for (int i = 0; i < 20; i++) {
    //            public string ip="127.0.0.1:9080";
    //             public string name="1区 马达加斯加";
    //public int count=100;
            string ip = "127.0.0.1:9080";
            string name = (i + 1) + "区 马达加斯加";
            int count = Random.Range(0, 100);
            GameObject go = null;
            if (count > 50) {
                //火爆
                go = NGUITools.AddChild(serverlistGrid.gameObject, serveritemRed);
            } else {
                //流畅
                go = NGUITools.AddChild(serverlistGrid.gameObject, serveritemGreen);
            }
            ServerProperty sp = go.GetComponent<ServerProperty>();
            sp.ip = ip;
            sp.name = name;
            sp.count = count;

            serverlistGrid.AddChild(  go.transform );
        }

        haveInitServerlist = true;
    }
    //显示服务器列表页面
    public void OnServerselect(GameObject serverGo) {
        sp = serverGo.GetComponent<ServerProperty>();
        //serverSelectedGo.GetComponent<UISprite>().spriteName = serverGo.GetComponent<UISprite>().spriteName;
        serverSelectedGo.GetComponent<UIButton>().normalSprite = serverGo.GetComponent<UIButton>().normalSprite;
        serverSelectedGo.GetComponent<UIButton>().hoverSprite = serverGo.GetComponent<UIButton>().hoverSprite;
        serverSelectedGo.GetComponent<UIButton>().pressedSprite = serverGo.GetComponent<UIButton>().pressedSprite;
        serverSelectedGo.GetComponent<UIButton>().disabledSprite = serverGo.GetComponent<UIButton>().disabledSprite;
        serverSelectedGo.transform.Find("Label").GetComponent<UILabel>().text = sp.name;
    }
    //服务器列表隐藏  显示开始界面
    public void OnServerpanelClose(){
        //隐藏服务器列表
        serverpanelTween.PlayReverse();
        StartCoroutine( HidePanel( serverpanelTween.gameObject ) );
        //显示开始界面
        startpanelTween.gameObject.SetActive(true);
        startpanelTween.PlayReverse();

        servernameLabelStart.text = sp.name;
    }
    //创建角色页面 点击选择角色
    public void OnCharacterClick(GameObject go)
    {
        if (go == characterselected)
            return;
        iTween.ScaleTo(go, new Vector3(1.5f, 1.5f, 1.5f), 0.5f);
        if(characterselected!=null)
        {
            iTween.ScaleTo(characterselected, new Vector3(1.0f, 1.0f, 1.0f), 0.5f);
            
        }
        characterselected = go;
    }

    //更换角色按钮
    public void OnButtonChangeCharacterClick()
    {
        //隐藏自身面板
        characterselectTweenPos.PlayReverse();
        HidePanel(characterselectTweenPos.gameObject);

        //显示展示角色面板
        charactershowTweenPos.gameObject.SetActive(true);
        charactershowTweenPos.PlayForward();
    }

    //角色展示界面-确定按钮单击事件
    public void OnCharactershowButtonSureClick()
    {
        //判断姓名是否输入正确
        //TODO
        //判断是否选择角色
        //TODO

        int index = -1;
        for (int i = 0; i < characterArry.Length; i++)
        {
            if(characterselected==characterArry[i])
            {
                index = i;
                break;
            }
        }
        if(index==-1)
        {
            return;
        }
        //销毁现有的角色
        Destroy(characterSelectedParent.GetComponentInChildren<Animation>().gameObject);
        //创建新的角色
        GameObject go = GameObject.Instantiate(characterSelectedArray[index], Vector3.zero, Quaternion.identity) as GameObject;
        go.transform.SetParent(characterSelectedParent);
        go.transform.localPosition = Vector3.zero;
        go.transform.localRotation = Quaternion.identity;
        go.transform.localScale = new Vector3(1, 1, 1);
        //更新角色名字和等级
        nameLabelCharacterSelected.text = characternameInput.value;
        levelLabelCharacterSelected.text = "Lv.1";
        OnCharactershowButtonBackClick(); 
    }
    //角色展示界面-返回按钮单击事件
    public void OnCharactershowButtonBackClick()
    {
        charactershowTweenPos.PlayReverse();
        HidePanel(charactershowTweenPos.gameObject);

        characterselectTweenPos.gameObject.SetActive(true);
        characterselectTweenPos.PlayForward();
    }

}