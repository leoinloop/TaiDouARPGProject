﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class PlayerAttack : MonoBehaviour {

    private Dictionary<string, PlayerEffect> effectDict = new Dictionary<string, PlayerEffect>();
    public PlayerEffect[] effectArray;
    public float distanceAttackForwad = 2;
    public float distanceAttackAround = 4;
    public int hp = 1000;
    private GameObject hudTextGameObject;
    private HUDText hudText;
    private Transform damageShowPoint;
    public int[] damageArray = new int[] { 20, 30, 30, 30 };
    public enum AttackRange
    {
        Forward,
        Around
    }

    private Animator anim; 

    void Start()
    {
        PlayerEffect[] peArray = this.GetComponentsInChildren<PlayerEffect>();
        foreach (PlayerEffect pe in peArray)
        {
            effectDict.Add(pe.gameObject.name, pe);
        }
        foreach (PlayerEffect pe in effectArray)
        {
            effectDict.Add(pe.gameObject.name, pe);
        }
        anim = this.GetComponent<Animator>();
        damageShowPoint = transform.Find("DamageShowPoint").transform;

        hudTextGameObject = HpBarManager._instance.GetHudText(damageShowPoint.gameObject);
        hudText = hudTextGameObject.GetComponent<HUDText>();
    }

   
	
    //0  normal skill1 skill2 skill3
    //1 effect name
    //2 sound name
    //3 move forward 帧向前移动
    //4 jump height
    void Attack(string args)
    {
        //1.show effect
        string[] proArray = args.Split(',');
        //print("args in Attack:" + args);


        string effectName = proArray[1];
        ShowPlayerEffect(effectName);
        //2.play sound
        string soundName = proArray[2];
        SoundManager._instance.Play(soundName);
        //Move Forward  控制前冲效果
        float moveForward = float.Parse(proArray[3]);
        if (moveForward > 0.1f)
        {
            iTween.MoveBy(gameObject, Vector3.forward * moveForward, 0.3f);
        }
        string posType = proArray[0];
        
        if (posType == "normal")
        {
            ArrayList array = GetEnemyAttackRange(AttackRange.Forward);


            foreach (GameObject go in array)
            {
                go.SendMessage("TakeDamage", damageArray[0] + "," + proArray[3] + "," + proArray[4]);
            }
        }
    }

    void PlaySound(string soundName)
    {
        SoundManager._instance.Play(soundName);
    }

    //0  normal skill1 skill2 skill3
    //1 move forward
    //2 jump height
    void SkillAttack(string args)
    {
        string[] proArray = args.Split(',');
        string posType = proArray[0];
        if (posType == "skill1")
        {
            ArrayList array = GetEnemyAttackRange(AttackRange.Forward);
            //array.AddRange(GetEnemyAttackRange(AttackRange.Around));

            foreach (GameObject go in array)
            {

                go.SendMessage("TakeDamage", damageArray[1] + "," + proArray[1] + "," + proArray[2]);
            }
        }else if (posType == "skill2")
        {
            
            ArrayList array = GetEnemyAttackRange(AttackRange.Forward);
            //array.AddRange(GetEnemyAttackRange(AttackRange.Around));

            foreach (GameObject go in array)
            {
                go.SendMessage("TakeDamage", damageArray[2] + "," + proArray[1] + "," + proArray[2]);
            }
        }else if (posType == "skill3")
        {

            ArrayList array = GetEnemyAttackRange(AttackRange.Forward);
            //array.AddRange(GetEnemyAttackRange(AttackRange.Around));


            foreach (GameObject go in array)
            {
                go.SendMessage("TakeDamage", damageArray[3] + "," + proArray[1] + "," + proArray[2] );
            }
        }
    }

    private void ShowPlayerEffect(string effectName)
    {
        PlayerEffect pe;
        if(effectDict.TryGetValue(effectName,out pe))
        {
            pe.Show();
        }
    }

    void ShowEffectDevilHand()
    {
        
        string effectName = "DevilHandMobile";
        PlayerEffect pe;
        effectDict.TryGetValue(effectName, out pe);
        ArrayList array = GetEnemyAttackRange(AttackRange.Forward);
        foreach(GameObject go in array)
        {
            RaycastHit hit;
            bool collider = Physics.Raycast(go.transform.position + Vector3.up, Vector3.down, out hit, LayerMask.GetMask("Ground"));
            if (collider)
            {
                GameObject.Instantiate(pe, hit.point+Vector3.left*1.3f, Quaternion.identity);
            }
        }
    }

    void ShowEffectSelfToTarget(string effectName)
    {
        //print("ShowEffect to Target");
        PlayerEffect pe;
        effectDict.TryGetValue(effectName, out pe);
        ArrayList array = GetEnemyAttackRange(AttackRange.Around);
        array.AddRange(GetEnemyAttackRange(AttackRange.Forward));
        foreach (GameObject go in array)
        {
                GameObject goEffect = (GameObject.Instantiate(pe) as PlayerEffect).gameObject;
                goEffect.transform.position = transform.position + Vector3.up;
                goEffect.GetComponent<EffectSettings>().Target = go;
        }
    }

    void ShowEffectToTarget(string effectName)
    {
        //print("ShowEffect to Target");
        PlayerEffect pe;
        effectDict.TryGetValue(effectName, out pe);
        ArrayList array = GetEnemyAttackRange(AttackRange.Forward);
        //array.AddRange(GetEnemyAttackRange(AttackRange.Around));
        foreach (GameObject go in array)
        {
            RaycastHit hit;
            bool collider = Physics.Raycast(go.transform.position + Vector3.up, Vector3.down, out hit, LayerMask.GetMask("Ground"));
            if (collider)
            {
                GameObject goEffect = (GameObject.Instantiate(pe) as PlayerEffect).gameObject;
                goEffect.transform.position = hit.point;
            }
        }
    }

    //得到在攻击范围内的敌人
    ArrayList GetEnemyAttackRange(AttackRange attackRange)
    {
        ArrayList arrayList = new ArrayList();
        if (attackRange == AttackRange.Forward)
        {
            foreach (GameObject go in TransciptManager._instance.enemyList)
            {
                if (go.gameObject == null) return arrayList ;
                Vector3 pos = transform.InverseTransformPoint(go.transform.position);     //敌人的世界坐标转换成主角的局部坐标
                if (pos.z > -0.5f)
                {
                    float distance = Vector3.Distance(Vector3.zero, pos);
                    if (distance < distanceAttackForwad)
                    {
                        arrayList.Add(go);
                    }
                }
            }
        }
        else
        {
            foreach (GameObject go in TransciptManager._instance.enemyList)
            {
                    float distance = Vector3.Distance(Vector3.zero, go.transform.position);
                    if (distance < distanceAttackAround)
                    {
                        arrayList.Add(go);
                    }
            }
        }
        return arrayList;
    }

    void TakeDamage(int damage)
    {
        if (hp <= 0) return;
            this.hp -= damage;
        //播放受到攻击的动画
        int random = UnityEngine.Random.Range(0, 100);
        if (random < damage)
        {
            anim.SetTrigger("TakeDamage");
        }
        //显示血量减少
        hudText.Add("-" + damage, Color.red, 0.3f);
        //屏幕上血红特效的显示
        BloodScreen.Instance.Show();

    }
}
