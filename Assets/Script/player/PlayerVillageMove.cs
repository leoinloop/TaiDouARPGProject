﻿using UnityEngine;
using System.Collections;

public class PlayerVillageMove : MonoBehaviour {


    public float velocity = 5;
    private Rigidbody m_Rigidbody;
    private NavMeshAgent agent;

    void Awake()
    {
        m_Rigidbody = transform.GetComponent<Rigidbody>();
        agent = this.GetComponent<NavMeshAgent>();
    }
	
	// Update is called once per frame
	void Update () {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        Vector3 vel = m_Rigidbody.velocity;
        m_Rigidbody.velocity = new Vector3(-h * velocity, vel.y, -v * velocity);

        if (Mathf.Abs(h) > 0.05f || Mathf.Abs(v) > 0.05f)
        {
            m_Rigidbody.velocity = new Vector3(-h * velocity, vel.y, -v*velocity);
            transform.rotation = Quaternion.LookRotation(new Vector3(-h, 0, -v));
        }else
        {
            if (agent.enabled == false)
            {
                m_Rigidbody.velocity = Vector3.zero;
            }
        }
        //if (agent.enabled)
        //{
        //    m_Rigidbody.rotation = Quaternion.LookRotation(agent.velocity);
        //}
    }
}
