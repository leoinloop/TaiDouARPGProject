﻿using UnityEngine;
using System.Collections;

public class PlayerAutoMove : MonoBehaviour {

    private Animator anim;
    private NavMeshAgent agent;
    public float minDistance =5;
    private Rigidbody m_Rigidbody;

	void Start () {
        agent = this.GetComponent<NavMeshAgent>();
        anim = this.GetComponent<Animator>();
        m_Rigidbody = this.GetComponent<Rigidbody>();

    }
	
	void Update () {
        if (agent.enabled)
        {
            if (Mathf.Abs(agent.remainingDistance) < minDistance && agent.remainingDistance != 0)
            {
                           
                agent.Stop();
                agent.enabled = false;
                TaskManager._instance.OnArrriveDestination();
                anim.SetBool("Move", false);
            }
            else
            {
                anim.SetBool("Move", true);
            }
        }

        
	}

    public void SetDestination(Vector3 targetPos)
    {
        //print(targetPos + "targetPos");
        agent.enabled = true;
        agent.SetDestination(targetPos);
    }
}
