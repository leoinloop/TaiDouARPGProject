﻿using UnityEngine;
using System.Collections;

public class PlayerVillageAnimation : MonoBehaviour {

    private Animator anim;
    private Rigidbody m_Rigidbody;


    void Start () {
        anim = this.GetComponent<Animator>();
        m_Rigidbody = this.GetComponent<Rigidbody>();
	}
	
	void Update () {

        if (m_Rigidbody.velocity.magnitude > 0.001f)
        {
            anim.SetBool("Move", true);
        }
        else
        {
            anim.SetBool("Move", false);
        }
        
	}

    
}
