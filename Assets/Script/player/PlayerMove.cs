﻿using UnityEngine;
using System.Collections;

public class PlayerMove : MonoBehaviour {

    public float velocity = 5;
    private Animator anim;
    private Transform m_Transform;

    void Start()
    {
        anim = this.GetComponent<Animator>();
        m_Transform = this.GetComponent<Transform>();
    }

	void Update () {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        Vector3 nowVel = gameObject.GetComponent<Rigidbody>().velocity;

        if (Mathf.Abs(h)>0.05f||Mathf.Abs(v)>0.05f)
        {
            anim.SetBool("Move", true);
            if (anim.GetCurrentAnimatorStateInfo(1).IsName("Empty State"))
            {
                gameObject.GetComponent<Rigidbody>().velocity = new Vector3(velocity * h, nowVel.y, v * velocity);
                transform.LookAt(new Vector3(h, 0, v) + m_Transform.position);
            }
            else
            {
                gameObject.GetComponent<Rigidbody>().velocity = new Vector3(0, nowVel.y, 0);

            }
        }
        else
        {
            anim.SetBool("Move", false);
            gameObject.GetComponent<Rigidbody>().velocity = new Vector3(0 , nowVel.y,0);

        }
    }
}
