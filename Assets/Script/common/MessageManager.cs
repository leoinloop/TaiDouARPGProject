﻿using UnityEngine;
using System.Collections;

public class MessageManager : MonoBehaviour {

    public static MessageManager _instance;
    private UILabel messageLabel;           //金币不足，无法升级Label
    private TweenAlpha tween;               //TweenAlpha 弹出信息显示
    private bool isSetActive = true ;       //弹出信息是否显示

    void Awake()
    {
        _instance = this;
        messageLabel = transform.Find("Label").GetComponent<UILabel>();
        tween = this.GetComponent<TweenAlpha>();

        EventDelegate ed = new EventDelegate(this, "OnTweenFinished");
        tween.onFinished.Add(ed);

        gameObject.SetActive(false);
    }
	
    

    /// <summary>
    /// 展示显示信息框
    /// </summary>
    /// <param name="time">显示时间</param>
    public void ShowMessage(string message, float time=1)
    {
        gameObject.SetActive(true);     //首先把物体设置可见
        StartCoroutine(Show(message, time));        //开启显示信息协程
    }

    /// <summary>
    /// 弹出金币不足无法升级提示
    /// </summary>
    /// <param name="message">传递信息参数</param>
    /// <param name="time">弹出信息显示时间</param>
    /// <returns></returns>
    IEnumerator Show(string message, float time)
    {        
        isSetActive = true;
        tween.PlayForward();
        messageLabel.text = message;

        yield return new WaitForSeconds(time);
        isSetActive = false;
        tween.PlayReverse();
    }

    /// <summary>
    /// 监视一旦信息显示完成时间超过time则隐藏信息显示面板
    /// </summary>
    public void OnTweenFinished()
    {
        if (isSetActive == false)
        {
            gameObject.SetActive(false);
        }
    }
}
