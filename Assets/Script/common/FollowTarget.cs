﻿using UnityEngine;
using System.Collections;

public class FollowTarget : MonoBehaviour {

    public Vector3 offset;
    private Transform playerBip;
    public float smoothing = 1;

	void Start () {
        playerBip = GameObject.FindGameObjectWithTag("Player").transform.Find("Bip01");
	}
	
	void FixedUpdate () {
        //transform.position = playerBip.position + offset;
        Vector3 targetPos = playerBip.position + offset;
        transform.position = Vector3.Lerp(transform.position, targetPos, smoothing * Time.deltaTime);
	}
}
