﻿using UnityEngine;
using System.Collections;

/// <summary>
/// 游戏控制
/// </summary>
public class GameController : MonoBehaviour {
    
    //返回获得目标等级所需的经验
    public static int GetRequireExpByLevel(int level)
    {
        //等差数列
        return (int)((level - 1) * (100f + (100f + 10f * (level - 2f))) / 2);
    }
}
