﻿using UnityEngine;
using System.Collections;
using System;

//信息类型
public enum InfoType
{
    Name,                       //名字
    HeadPortrait,               //头像
    Level,                      //等级
    Power,                      //攻击力
    Exp,                        //经验
    Diamond,                    //钻石
    Coin,                       //金币
    Energy,                     //体力
    Toughen,                    //历练
    HP,
    Damage,
    Equip,
    All                         //以上全部
}

public enum PlayerType
{
    Warrior,
    FemaleAssassin
}

public class PlayerInfo : MonoBehaviour {


    public static PlayerInfo _instance;
    //姓名
    //头像
    //等级
    //战斗力
    //经验数
    //钻石数
    //金币数
    //体力数
    //历练数
    #region property
    private string _name;
    private string _headPortrait;
    private int _level = 1;
    private int _power = 1;
    private int _exp = 0;
    private int _diamond;
    private int _coin;
    private int _energy;
    private int _toughen;
    
    private int _hp;
    private int _damage;
    #endregion
    //private int _helmID=0;          //默认值为0表示未穿戴装备
    private PlayerType _playerType;
    //private int _clothID=0;
    //private int _weaponID=0;
    //private int _shoesID=0;
    //private int _necklaceID=0;
    //private int _braceletID=0;
    //private int _ringID=0;
    //private int _wingID=0;

    public InventoryItem helmInventoryItem;
    public InventoryItem clothInventoryItem;
    public InventoryItem weaponInventoryItem;
    public InventoryItem shoesInventoryItem;
    public InventoryItem necklaceInventoryItem;
    public InventoryItem braceletInventoryItem;
    public InventoryItem ringInventoryItem;
    public InventoryItem wingInventoryItem;

    

    public float energyTimer = 0;       //体力恢复计数器
    public float toughenTimer = 0;      //历练恢复计数器

    //委托事件--角色信息改变事件
    public delegate void OnPlayerInfoChangedEvent(InfoType type);
    public event OnPlayerInfoChangedEvent OnPlayerInfoChanged;

    #region get set method
    public int HP
    {
        get { return _hp; }
        set
        {
            _hp = value;
        }
    }
    public int Damage
    {
        get { return _damage; }
        set { _damage = value; }
    }
    public PlayerType PlayerType
    {
        get
        {
            return _playerType;
        }set
        {
            _playerType = value;
        }
    }
    //public int HelmID
    //{
    //    get { return _helmID; }
    //    set { _helmID = value; }
    //}
    //public int ClothID
    //{
    //    get { return _clothID; }
    //    set { _clothID = value; }
    //}
    //public int WeaponID
    //{
    //    get { return _weaponID; }
    //    set { _weaponID = value; }
    //}
    //public int ShoesID
    //{
    //    get { return _shoesID; }
    //    set { _shoesID = value; }
    //}
    //public int NecklaceID
    //{
    //    get { return _necklaceID; }
    //    set { _necklaceID = value; }
    //}
    //public int BraceletID
    //{
    //    get { return _braceletID; }
    //    set { _braceletID = value; }
    //}
    //public int WingID
    //{
    //    get { return _wingID; }
    //    set { _wingID = value; }
    //}
    //public int RingID
    //{
    //    get { return _ringID; }
    //    set { _ringID = value; }
    //}

    public string Name
    {
        get { return _name; }
        set { _name = value; }
    }
    public string HeadPortrait
    {
        get { return _headPortrait; }
        set { _headPortrait = value; }
    }
    public int Level
    {
        get { return _level; }
        set { _level = value; }
    }
    public int Power
    {
        get { return _power; }
        set { _power = value; }
    }
    public int Exp
    {
        get { return _exp; }
        set { _exp = value; }
    }
    public int Diamond
    {
        get { return _diamond; }
        set { _diamond = value; }
    }
    public int Coin
    {
        get { return _coin; }
        set { _coin = value; }
    }
    public int Energy
    {
        get { return _energy; }
        set { _energy = value; }
    }
    public int Toughen
    {
        get { return _toughen; }
        set { _toughen = value; }
    }
    void Update()
    {
        if (this.Energy < 100)
        {
            energyTimer += Time.deltaTime;
            if(energyTimer>60)
            {
                Energy += 1;
                energyTimer -= 60;
                OnPlayerInfoChanged(InfoType.Energy);
            }
        }else
        {
            this.energyTimer = 0;
        }

        if(this.Toughen<60)
        {
            toughenTimer += Time.deltaTime;
            if(toughenTimer>60)
            {
                Toughen += 1;
                toughenTimer -= 60;
                OnPlayerInfoChanged(InfoType.Toughen);
            }
        }else
        {
            toughenTimer = 0;
        }
    }
    #endregion
    #region unity event
    void Awake()
    {
        _instance = this;           //单例
    }

    void Start()
    {
        Init();
    }
    #endregion

    //初始化角色信息
    void Init()
    {
        this.Coin = 9870;
        this.Diamond = 1234;
        this.Energy = 78;
        this.Exp = 123;
        this.HeadPortrait = "头像底板男性";
        this.Level = 12;
        this.Name = "周美灵";
        this.Power = 1745;
        this.Toughen = 34;

        
        //this.BraceletID = 1001;
        //this.WingID = 1002;
        //this.RingID = 1003;
        //this.ClothID = 1004;
        //this.HelmID = 1005;
        //this.WeaponID = 1006;
        //this.NecklaceID = 1007;
        //this.ShoesID = 1008;

        InitHPDamagePower();    //初始化生命值 伤害 力量

        OnPlayerInfoChanged(InfoType.All);
    }

    //初始化HP DAmage Power 根据策划公式得出
    private void InitHPDamagePower()
    {
        this.HP = this.Level * 100;
        this.Damage = this.Level * 50;
        this.Power = this.HP + this.Damage;
        //PutonEquip(BraceletID);
        //PutonEquip(WingID);
        //PutonEquip(RingID);
        //PutonEquip(ClothID);
        //PutonEquip(HelmID);
        //PutonEquip(WeaponID);
        //PutonEquip(NecklaceID);
        //PutonEquip(ShoesID);
    }

    /// <summary>
    /// 取得需要个数的金币数（装备升级）
    /// </summary>
    /// <param name="count"></param>
    public bool GetCoin(int count)
    {
        if (Coin >= count)
        {
            Coin -= count;          //角色拥有金币-升级装备所持金币
            OnPlayerInfoChanged(InfoType.Coin);
            return true;
        }
        return false;
    }

    /// <summary>
    /// 出售存钱
    /// </summary>
    /// <param name="count"></param>
    public void AddCoin(int count)
    {
        this.Coin += count;
        OnPlayerInfoChanged(InfoType.Coin);
    }

    public void DressOn(InventoryItem it)
    {
        it.IsDressed = true;
        //首先检测有没有相同类型的装备
        bool isDressed = false;
        InventoryItem inventoryItemDressed = null;
        switch (it.Inventroy.EquipTYPE)
        {
            case EquipType.Bracelet:
                if (braceletInventoryItem != null)
                {
                    isDressed = true;
                    inventoryItemDressed = braceletInventoryItem;
                }
                braceletInventoryItem = it;
                break;
            case EquipType.Cloth:
                if (clothInventoryItem != null)
                {
                    isDressed = true;
                    inventoryItemDressed = clothInventoryItem;
                }
                clothInventoryItem = it;
                break;
            case EquipType.Helm:
                if(helmInventoryItem!=null)
                {
                    isDressed = true;
                    inventoryItemDressed = helmInventoryItem;
                }
                helmInventoryItem = it;
                break;
            case EquipType.Neckiace:
                if (necklaceInventoryItem != null)
                {
                    isDressed = true;
                    inventoryItemDressed = necklaceInventoryItem;
                }
                necklaceInventoryItem = it;
                break;
            case EquipType.Weapon:
                if (weaponInventoryItem != null)
                {
                    isDressed = true;
                    inventoryItemDressed = weaponInventoryItem;
                }
                weaponInventoryItem = it;
                break;
            case EquipType.Shoes:
                if (shoesInventoryItem != null)
                {
                    isDressed = true;
                    inventoryItemDressed = shoesInventoryItem;
                }
                shoesInventoryItem = it;
                break;
            case EquipType.Wing:
                if (wingInventoryItem != null)
                {
                    isDressed = true;
                    inventoryItemDressed = wingInventoryItem;
                }
                wingInventoryItem = it;
                break;
            case EquipType.Ring:
                if (ringInventoryItem != null)
                {
                    isDressed = true;
                    inventoryItemDressed = ringInventoryItem;
                }
                ringInventoryItem = it;
                break;
        }
        //有
        if (isDressed)
        {
            inventoryItemDressed.IsDressed = false;
            InventoryUI._instance.AddInventoryItem(inventoryItemDressed);
        }
        //穿上
        OnPlayerInfoChanged(InfoType.Equip);

        //把已经存在的脱掉 放到背包里面
        //没有
        //直接穿上
    }

    public void DressOff(InventoryItem it)
    {
        switch (it.Inventroy.EquipTYPE)
        {
            case EquipType.Bracelet:
                if (braceletInventoryItem != null)
                {
                    braceletInventoryItem = null;
                }
                break;
            case EquipType.Cloth:
                if (clothInventoryItem != null)
                {
                    clothInventoryItem = null;
                }
                break;
            case EquipType.Helm:
                if (helmInventoryItem != null)
                {
                    helmInventoryItem = null;
                }
                break;
            case EquipType.Neckiace:
                if (necklaceInventoryItem != null)
                {
                    necklaceInventoryItem = null;
                }
                break;
            case EquipType.Weapon:
                if (weaponInventoryItem != null)
                {
                    weaponInventoryItem = null;
                }
                break;
            case EquipType.Shoes:
                if (shoesInventoryItem != null)
                {
                    shoesInventoryItem = null;
                }
                break;
            case EquipType.Wing:
                if (wingInventoryItem != null)
                {
                    wingInventoryItem = null;
                }
                break;
            case EquipType.Ring:
                if (ringInventoryItem != null)
                {
                    ringInventoryItem = null;
                }
                break;
        }

        it.IsDressed = false;
        InventoryUI._instance.AddInventoryItem(it);
        OnPlayerInfoChanged(InfoType.Equip);
    }

    public void InventoryUse(InventoryItem it,int count)
    {
        //使用效果+体力
        //TODO
        //处理物品使用后是否还存在
        it.Count -= count;
        if (it.Count <= 0)
        {
            InventoryManager._instance.inventoryItemList.Remove(it);
        }
    }

    public int GetOverPower()
    {
        float power = this.Power;
        if (helmInventoryItem != null)
        {
            power += helmInventoryItem.Inventroy.Power*(1+(helmInventoryItem.Level-1)/10f);
        }
        if (clothInventoryItem != null)
        {
            power += clothInventoryItem.Inventroy.Power * (1 + (clothInventoryItem.Level - 1) / 10f);
        }
        if (weaponInventoryItem != null)
        {
            power += weaponInventoryItem.Inventroy.Power * (1 + (weaponInventoryItem.Level - 1) / 10f);
        }
        if (shoesInventoryItem != null)
        {
            power += shoesInventoryItem.Inventroy.Power * (1 + (shoesInventoryItem.Level - 1) / 10f);
        }
        if (necklaceInventoryItem != null)
        {
            power += necklaceInventoryItem.Inventroy.Power * (1 + (necklaceInventoryItem.Level - 1) / 10f);
        }
        if (braceletInventoryItem != null)
        {
            power += braceletInventoryItem.Inventroy.Power * (1 + (braceletInventoryItem.Level - 1) / 10f);
        }
        if (ringInventoryItem != null)
        {
            power += ringInventoryItem.Inventroy.Power * (1 + (ringInventoryItem.Level - 1) / 10f);
        }
        if (wingInventoryItem != null)
        {
            power += wingInventoryItem.Inventroy.Power * (1 + (wingInventoryItem.Level - 1) / 10f);
        }
        return (int)power;
    }

    //改变角色名字
    public void ChangeName(string name)
    {
        this.Name = name;
        OnPlayerInfoChanged(InfoType.Name);
    }

    //穿上装备
    void PutonEquip(int id)
    {
        if (id == 0) return;            //id=0默认值则为穿戴装备直接返回
        Inventory inventory = null;
        bool isExit = InventoryManager._instance.inventoryDict.TryGetValue(id, out inventory);

        this.HP += inventory.HP;
        this.Damage += inventory.Damage;
        this.Power += inventory.Power;
    }

    //脱下装备
    void PutoffEquip(int id)
    {
        if (id == 0) return;        //id=0默认值则为穿戴装备直接返回
        Inventory inventory = null;
        InventoryManager._instance.inventoryDict.TryGetValue(id, out inventory);
        this.HP -= inventory.HP;
        this.Damage -= inventory.Damage;
        this.Power -= inventory.Power;
    }

    
}
